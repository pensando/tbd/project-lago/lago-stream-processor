package main

import (
	"github.com/Jeffail/benthos/v3/lib/service"
	_ "gitlab.com/pensando/tbd/project-lago/lago-stream-processor/processor"
	_ "go.uber.org/automaxprocs"
)

func main() {
	service.Run()
}
