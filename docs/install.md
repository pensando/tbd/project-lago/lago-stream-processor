#######Cluster Creation
1. Create 3 VMs
2. Configure VMs:
    sudo swapoff --all
    sudo sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab
3. Copy keys
    ssh-keygen -f ~/.ssh/pen-key-ecdsa -t ecdsa -b 521
    ssh-copy-id -i ~/.ssh/pen-key-ecdsa ubuntu@10.29.75.161
    ssh-copy-id -i ~/.ssh/pen-key-ecdsa ubuntu@10.29.75.162
    ssh-copy-id -i ~/.ssh/pen-key-ecdsa ubuntu@10.29.75.163
4. Add passwordless sudo to the main user(ubuntu)
    sudo visudo
    add UBUNTU ALL=(ALL) NOPASSWD: ALL
    at the bottom of the file.
5. Run k3s install ./bootstrap.sh
6. Set KUBECONFIG
    export KUBECONFIG=/Users/sqr/pensando/env/kubeconfig
7. Validate cluster is running
    kubectl get nodes
    NAME       STATUS   ROLES         AGE    VERSION
    penkube1   Ready    etcd,master   105s   v1.19.5+k3s2
    penkube2   Ready    etcd,master   76s    v1.19.5+k3s2
    penkube3   Ready    etcd,master   47s    v1.19.5+k3s2
8. Create Docker Image Pull Secrets
kubectl create secret docker-registry pensando-image-creds --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-349500 --docker-password=sufyMzq4GAaebsLjcBYU --docker-email=deploy@pensando.io
