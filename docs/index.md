# Welcome

Use the following table of contents to find what you are looking for

- [What is Lago all about?](./about.md)
- [Lago Architecture](./architecture.md)
- [K8s Implementation for Lago](./k8s.md)
- [Installation](./installation.md)
- [Useful Resources](./resources.md)

![ipfix](./img/IPfix Ingest.png)

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
