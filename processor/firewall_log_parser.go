package processor

import (
	"bytes"
	"errors"
	"fmt"
	"time"

	"github.com/Jeffail/benthos/v3/lib/log"
	"github.com/Jeffail/benthos/v3/lib/message"
	"github.com/Jeffail/benthos/v3/lib/metrics"
	"github.com/Jeffail/benthos/v3/lib/processor"
	"github.com/Jeffail/benthos/v3/lib/types"
	syslog "github.com/influxdata/go-syslog/v3"
	"github.com/influxdata/go-syslog/v3/rfc5424"
)

//------------------------------------------------------------------------------

func init() {
	processor.RegisterPlugin(
		"firewall-log-parser",
		func() interface{} {
			return NewFirewallLogProcessorConfig()
		},
		func(
			iconf interface{},
			mgr types.Manager,
			logger log.Modular,
			stats metrics.Type,
		) (types.Processor, error) {
			conf, ok := iconf.(*FirewallLogProcessorConfig)
			if !ok {
				return nil, errors.New("Failed to cast config for firewall-log-parser processor.")
			}
			return NewFirewallLogProcessor(*conf, logger, stats)
		},
	)
	processor.DocumentPlugin(
		"firewall-log-parser",
		`Parses a DSC fw log messages.`,
		nil,
	)
}

// NewFirewallLogProcessorConfig creates a config with default values.
func NewFirewallLogProcessorConfig() *FirewallLogProcessorConfig {
	return &FirewallLogProcessorConfig{}
}

// FirewallLogProcessorConfig contains config fields for our plugin type.
type FirewallLogProcessorConfig struct {
}

//------------------------------------------------------------------------------

// FirewallLogProcessor is a processor that reads all mqtt messages.
type FirewallLogProcessor struct {
	specversion     string
	source          string
	messageType     string
	datacontenttype string
	dataencoding    string
	parser          syslog.Machine
	log             log.Modular
	stats           metrics.Type
	mCount          metrics.StatCounter
	mDropped        metrics.StatCounter
	mBatchSent      metrics.StatCounter
	mFiltered       metrics.StatCounter
	mSent           metrics.StatCounter
	mErr            metrics.StatCounter
}

// NewFirewallLogProcessor returns a FirewallLogProcessor processor.
func NewFirewallLogProcessor(
	conf FirewallLogProcessorConfig, log log.Modular, stats metrics.Type,
) (types.Processor, error) {
	m := &FirewallLogProcessor{
		parser:     rfc5424.NewParser(rfc5424.WithBestEffort()),
		log:        log,
		stats:      stats,
		mCount:     stats.GetCounter("count"),
		mDropped:   stats.GetCounter("dropped"),
		mSent:      stats.GetCounter("sent"),
		mBatchSent: stats.GetCounter("batch.sent"),
		mErr:       stats.GetCounter("error"),
	}
	return m, nil
}

// ProcessMessage applies the processor to a message
func (m *FirewallLogProcessor) ProcessMessage(msg types.Message) ([]types.Message, types.Response) {
	m.mCount.Incr(1)
	newMsg := message.New(nil)
	msg.Iter(func(i int, part types.Part) error {
		//Syslog Processing
		replacedString := bytes.Replace(part.Get(), []byte("  - ["), []byte(" - - ["), -1)
		dataMap, err := m.parserRFC5424(replacedString)
		if err != nil {
			m.mErr.Incr(1)
			m.log.Debugf("Failed to parse syslog message: %v\n", err)
			processor.FlagFail(part)
			return err
		}

		m.log.Debugf("Parsed Syslog message: %v\n", dataMap)
		//Extraction of data we need - message and hostname
		part.Metadata().Set("dsckey", dataMap["hostname"].(string))
		part.Set([]byte(string(dataMap["message"].(string))))
		m.log.Debugf("Part Value: %v\n", part.Get())

		//parsing into array
		newParts, err := m.unarchiveArray(part)

		if err == nil {
			newMsg.Append(newParts...)
		} else {
			m.mErr.Incr(1)
			m.log.Errorf("Failed to unarchive message part: %v\n", err)
			newMsg.Append(part)
			processor.FlagFail(part)
		}
		return nil
	})

	m.mBatchSent.Incr(1)
	m.mSent.Incr(int64(newMsg.Len()))
	msgs := [1]types.Message{newMsg}
	return msgs[:], nil
}

// CloseAsync shuts down the processor and stops processing requests.
func (m *FirewallLogProcessor) CloseAsync() {
}

// WaitForClose blocks until the processor has closed down.
func (m *FirewallLogProcessor) WaitForClose(timeout time.Duration) error {
	return nil
}

func (m *FirewallLogProcessor) recover(p types.Part) {
	if r := recover(); r != nil {
		m.log.Errorf("Recovered from a message processing error.", r)
		processor.FlagFail(p)
		m.mDropped.Incr(1)
	}
}

func (m *FirewallLogProcessor) parserRFC5424(body []byte) (map[string]interface{}, error) {
	resGen, err := m.parser.Parse(body)
	if err != nil {
		return nil, err
	}
	res := resGen.(*rfc5424.SyslogMessage)

	resMap := make(map[string]interface{})
	if res.Message != nil {
		resMap["message"] = *res.Message
	}
	if res.Timestamp != nil {
		resMap["timestamp"] = res.Timestamp.Format(time.RFC3339Nano)
		// resMap["timestamp_unix"] = res.Timestamp().Unix()
	}
	if res.Facility != nil {
		resMap["facility"] = *res.Facility
	}
	if res.Severity != nil {
		resMap["severity"] = *res.Severity
	}
	if res.Priority != nil {
		resMap["priority"] = *res.Priority
	}
	if res.Version != 0 {
		resMap["version"] = res.Version
	}
	if res.Hostname != nil {
		resMap["hostname"] = *res.Hostname
	}
	if res.ProcID != nil {
		resMap["procid"] = *res.ProcID
	}
	if res.Appname != nil {
		resMap["appname"] = *res.Appname
	}
	if res.MsgID != nil {
		resMap["msgid"] = *res.MsgID
	}
	if res.StructuredData != nil {
		resMap["structureddata"] = *res.StructuredData
	}
	return resMap, nil
}

func (m *FirewallLogProcessor) unarchiveArray(part types.Part) ([]types.Part, error) {
	logArray, err := part.JSON()
	if err != nil {
		m.log.Errorf("Part error: %v\n", string(part.Get()))
		return nil, fmt.Errorf("failed to parse message into JSON array: %v", err)
	}

	jArray, ok := logArray.([]interface{})
	if !ok {
		m.log.Errorf("Part error: %v\n", string(part.Get()))
		return nil, fmt.Errorf("failed to parse message into JSON array: invalid type '%T'", jArray)
	}
	parts := make([]types.Part, len(jArray))
	for i, ele := range jArray {
		newPart := part.Copy()
		if err = newPart.SetJSON(ele); err != nil {
			return nil, fmt.Errorf("failed to marshal element into new message: %v", err)
		}
		parts[i] = newPart
	}
	return parts, nil
}
