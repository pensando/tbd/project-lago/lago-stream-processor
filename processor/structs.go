package processor

type CX10K struct {
	Source struct {
		IP      string `json:"ip"`
		Port    uint64 `json:"port"`
		Vlan    uint64 `json:"vlan"`
		Vrf     string `json:"vrf_uuid"`
		VrfName string `json:"vrf_name"`
	} `json:"source"`
	Destination struct {
		IP      string `json:"ip"`
		Port    uint64 `json:"port"`
		Vlan    uint64 `json:"vlan"`
		Vrf     string `json:"vrf_uuid"`
		VrfName string `json:"vrf_name"`
	} `json:"destination"`
	Dsx struct {
		DeviceName string `json:"device_name"`
		SerNum     string `json:"ser_num"`
		SwVer      string `json:"sw_ver"`
		Type       string `json:"type"`
		UnitID     string `json:"unit_id"`
	} `json:"dsx"`
	GenTime string `json:"gen_time"`
	Host    struct {
		IP string `json:"ip"`
	} `json:"host"`
	Hostname string `json:"hostname"`
	MsgID    string `json:"msg_id"`
	ProcID   string `json:"proc_id"`
	ProcName string `json:"proc_name"`
	Protocol uint64 `json:"protocol"`
	Sd       string `json:"sd"`
	Security struct {
		PolicyName  string `json:"policy_name"`
		DisplayName string `json:"policy_display_name"`
		PolicyID    string `json:"policy_id"`
		RuleID      string `json:"rule_id"`
		RuleName    string `json:"rule_name"`
		Encrypted   string `json:"encrypted"`
	} `json:"security"`
	Nat struct {
		SourceXltdIP string `json:"source_xltd_ip"`
		DestXltdIP   string `json:"dest_xltd_ip"`
		DestXltdPort uint64 `json:"dest_xltd_port"`
	} `json:"nat"`
	Session struct {
		Action     string `json:"action"`
		ID         string `json:"id"`
		IflowBytes uint64 `json:"iflow_bytes"`
		IflowPkts  uint64 `json:"iflow_pkts"`
		RflowBytes uint64 `json:"rflow_bytes"`
		RflowPkts  uint64 `json:"rflow_pkts"`
		State      string `json:"state"`
		Direction  string `json:"direction"`
		Create     string `json:"create_reason"`
		Close      string `json:"close_reason"`
	} `json:"session"`
	SyslogPriority uint8    `json:"syslog_priority"`
	SyslogProto    string   `json:"syslog_proto"`
	SyslogVersion  uint16   `json:"syslog_version"`
	Tags           []string `json:"tags"`
	Version        string   `json:"version"`
}
