package processor

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/Jeffail/benthos/v3/lib/log"
	"github.com/Jeffail/benthos/v3/lib/message"
	"github.com/Jeffail/benthos/v3/lib/metrics"
	"github.com/Jeffail/benthos/v3/lib/processor"
	"github.com/Jeffail/benthos/v3/lib/types"
	syslog "github.com/influxdata/go-syslog/v3"
	"github.com/influxdata/go-syslog/v3/rfc5424"
)

//------------------------------------------------------------------------------

func init() {
	processor.RegisterPlugin(
		"enterprise-log-splitter",
		func() interface{} {
			return NewEnterpriseLogSplitterConfig()
		},
		func(
			iconf interface{},
			mgr types.Manager,
			logger log.Modular,
			stats metrics.Type,
		) (types.Processor, error) {
			conf, ok := iconf.(*EnterpriseLogSplitterConfig)
			if !ok {
				return nil, errors.New("Failed to cast config for enterprise-log-splitter processor.")
			}
			return NewEnterpriseLogSplitter(*conf, logger, stats)
		},
	)
	processor.DocumentPlugin(
		"enterprise-log-splitter",
		`Splits switch log messages.`,
		nil,
	)
}

// NewEnterpriseLogSplitterConfig creates a config with default values.
func NewEnterpriseLogSplitterConfig() *EnterpriseLogSplitterConfig {
	return &EnterpriseLogSplitterConfig{}
}

// EnterpriseLogSplitterConfig contains config fields for our plugin type.
type EnterpriseLogSplitterConfig struct {
}

//------------------------------------------------------------------------------

// EnterpriseLogSplitter is a processor that reads all mqtt messages.
type EnterpriseLogSplitter struct {
	parser     syslog.Machine
	log        log.Modular
	stats      metrics.Type
	mCount     metrics.StatCounter
	mDropped   metrics.StatCounter
	mBatchSent metrics.StatCounter
	mFiltered  metrics.StatCounter
	mSent      metrics.StatCounter
	mErr       metrics.StatCounter
}

// NewEnterpriseLogSplitter returns a EnterpriseLogSplitter processor.
func NewEnterpriseLogSplitter(
	conf EnterpriseLogSplitterConfig, log log.Modular, stats metrics.Type,
) (types.Processor, error) {
	m := &EnterpriseLogSplitter{
		parser:     rfc5424.NewParser(rfc5424.WithBestEffort()),
		log:        log,
		stats:      stats,
		mCount:     stats.GetCounter("count"),
		mDropped:   stats.GetCounter("dropped"),
		mSent:      stats.GetCounter("sent"),
		mBatchSent: stats.GetCounter("batch.sent"),
		mErr:       stats.GetCounter("error"),
	}
	return m, nil
}

// ProcessMessage applies the processor to a message
func (m *EnterpriseLogSplitter) ProcessMessage(msg types.Message) ([]types.Message, types.Response) {
	m.mCount.Incr(1)
	newMsg := message.New(nil)
	msg.Iter(func(i int, part types.Part) error {
		m.log.Debugf("Received RFC5424 Syslog message: %s\n", part.Get())
		message, csv, err := m.parserRFC5424(part.Get())
		if err != nil {
			m.mErr.Incr(1)
			m.log.Debugf("Failed to parse RFC5424 syslog message: %v\n", err)
			processor.FlagFail(part)
			return err
		}
		m.log.Debugf("Parsed Syslog message: %v\n", message)
		splitMessages := strings.Split(csv, "\n")
		m.log.Debugf("Message contains %v submessages\n", len(splitMessages))
		for _, singleMessage := range splitMessages {
			if singleMessage == "" {
				continue
			}
			m.log.Debugf("Found Message: %v\n", singleMessage)
			cpy := Copy(message).(*rfc5424.SyslogMessage)
			cpy.SetMessage(singleMessage)
			newMessageSerialized, err := cpy.String()
			copiedPart := part.Copy()
			if err != nil {
				fmt.Errorf("failed to marshal element into new message: %v", err)
			}
			m.log.Debugf("New Message: %v\n", newMessageSerialized)
			copiedPart.Set([]byte(newMessageSerialized + "\n"))
			newMsg.Append(copiedPart)
		}
		return nil
	})

	m.mBatchSent.Incr(1)
	m.mSent.Incr(int64(newMsg.Len()))
	msgs := [1]types.Message{newMsg}
	return msgs[:], nil
}

// CloseAsync shuts down the processor and stops processing requests.
func (m *EnterpriseLogSplitter) CloseAsync() {
}

// WaitForClose blocks until the processor has closed down.
func (m *EnterpriseLogSplitter) WaitForClose(timeout time.Duration) error {
	return nil
}

func (m *EnterpriseLogSplitter) recover(p types.Part) {
	if r := recover(); r != nil {
		m.log.Errorf("Recovered from a message processing error.", r)
		processor.FlagFail(p)
		m.mDropped.Incr(1)
	}
}

func (m *EnterpriseLogSplitter) parserRFC5424(body []byte) (*rfc5424.SyslogMessage, string, error) {
	resGen, err := m.parser.Parse(body)
	if err != nil {
		return nil, "", err
	}
	res := resGen.(*rfc5424.SyslogMessage)

	if res.Message != nil {
		return res, *res.Message, nil
	}
	return res, "", nil
}
