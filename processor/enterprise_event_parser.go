package processor

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/Jeffail/benthos/v3/lib/log"
	"github.com/Jeffail/benthos/v3/lib/message"
	"github.com/Jeffail/benthos/v3/lib/metrics"
	"github.com/Jeffail/benthos/v3/lib/processor"
	"github.com/Jeffail/benthos/v3/lib/types"
	syslog "github.com/influxdata/go-syslog/v3"
	"github.com/influxdata/go-syslog/v3/rfc5424"
)

//------------------------------------------------------------------------------

func init() {
	processor.RegisterPlugin(
		"enterprise-log-parser",
		func() interface{} {
			return NewEnterpriseLogProcessorConfig()
		},
		func(
			iconf interface{},
			mgr types.Manager,
			logger log.Modular,
			stats metrics.Type,
		) (types.Processor, error) {
			conf, ok := iconf.(*EnterpriseLogProcessorConfig)
			if !ok {
				return nil, errors.New("Failed to cast config for enterprise-log-parser processor.")
			}
			return NewEnterpriseLogProcessor(*conf, logger, stats)
		},
	)
	processor.DocumentPlugin(
		"enterprise-log-parser",
		`Parses a switch log messages.`,
		nil,
	)
}

// NewEnterpriseLogProcessorConfig creates a config with default values.
func NewEnterpriseLogProcessorConfig() *EnterpriseLogProcessorConfig {
	return &EnterpriseLogProcessorConfig{}
}

// EnterpriseLogProcessorConfig contains config fields for our plugin type.
type EnterpriseLogProcessorConfig struct {
}

//------------------------------------------------------------------------------

// EnterpriseLogProcessor is a processor that reads all mqtt messages.
type EnterpriseLogProcessor struct {
	parser     syslog.Machine
	log        log.Modular
	stats      metrics.Type
	mCount     metrics.StatCounter
	mDropped   metrics.StatCounter
	mBatchSent metrics.StatCounter
	mFiltered  metrics.StatCounter
	mSent      metrics.StatCounter
	mErr       metrics.StatCounter
}

// NewEnterpriseLogProcessor returns a EnterpriseLogProcessor processor.
func NewEnterpriseLogProcessor(
	conf EnterpriseLogProcessorConfig, log log.Modular, stats metrics.Type,
) (types.Processor, error) {
	m := &EnterpriseLogProcessor{
		parser:     rfc5424.NewParser(rfc5424.WithBestEffort()),
		log:        log,
		stats:      stats,
		mCount:     stats.GetCounter("count"),
		mDropped:   stats.GetCounter("dropped"),
		mSent:      stats.GetCounter("sent"),
		mBatchSent: stats.GetCounter("batch.sent"),
		mErr:       stats.GetCounter("error"),
	}
	return m, nil
}

// ProcessMessage applies the processor to a message
func (m *EnterpriseLogProcessor) ProcessMessage(msg types.Message) ([]types.Message, types.Response) {
	m.mCount.Incr(1)
	newMsg := message.New(nil)
	msg.Iter(func(i int, part types.Part) error {
		m.log.Debugf("Received Syslog message: %s\n", part.Get())
		dataMap, err := m.parserRFC5424(part.Get())
		if err != nil {
			m.mErr.Incr(1)
			m.log.Debugf("Failed to parse rfc5424 syslog message: %v\n", err)
			processor.FlagFail(part)
			return err
		}
		m.log.Debugf("Parsed Syslog message: %v\n", dataMap)
		splitMessages := strings.Split(dataMap["message"].(string), "\n")
		m.log.Debugf("Message contains %v submessages\n", len(splitMessages))
		for _, str := range splitMessages {
			m.log.Debugf("Found Message: %v\n", str)
			if str == "" || str == " " || str == "\n" || len(str) < 3 {
				continue
			}
			newPart, err := m.parseCSV(str, dataMap)
			if err != nil {
				m.mErr.Incr(1)
				m.log.Errorf("Failed to parse syslog csv data in message: %v\n", err)
				processor.FlagFail(part)
				return err
			}
			m.log.Debugf("CreatedNewMessage: %s\n", newPart)
			copiedPart := part.Copy()
			if err = copiedPart.SetJSON(newPart); err != nil {
				fmt.Errorf("failed to marshal element into new message: %v", err)
			}
			newMsg.Append(copiedPart)
		}
		return nil
	})

	m.mBatchSent.Incr(1)
	m.mSent.Incr(int64(newMsg.Len()))
	msgs := [1]types.Message{newMsg}
	return msgs[:], nil
}

// CloseAsync shuts down the processor and stops processing requests.
func (m *EnterpriseLogProcessor) CloseAsync() {
}

// WaitForClose blocks until the processor has closed down.
func (m *EnterpriseLogProcessor) WaitForClose(timeout time.Duration) error {
	return nil
}

func (m *EnterpriseLogProcessor) recover(p types.Part) {
	if r := recover(); r != nil {
		m.log.Errorf("Recovered from a message processing error.", r)
		processor.FlagFail(p)
		m.mDropped.Incr(1)
	}
}

func (m *EnterpriseLogProcessor) parserRFC5424(body []byte) (map[string]interface{}, error) {
	resGen, err := m.parser.Parse(body)
	if err != nil {
		return nil, err
	}
	res := resGen.(*rfc5424.SyslogMessage)

	resMap := make(map[string]interface{})
	if res.Message != nil {
		resMap["message"] = *res.Message
	}
	if res.Timestamp != nil {
		resMap["timestamp"] = res.Timestamp.Format(time.RFC3339Nano)
		// resMap["timestamp_unix"] = res.Timestamp().Unix()
	}
	if res.Facility != nil {
		resMap["facility"] = *res.Facility
	}
	if res.Severity != nil {
		resMap["severity"] = *res.Severity
	}
	if res.Priority != nil {
		resMap["priority"] = *res.Priority
	}
	if res.Version != 0 {
		resMap["version"] = res.Version
	}
	if res.Hostname != nil {
		resMap["hostname"] = *res.Hostname
	}
	if res.ProcID != nil {
		resMap["procid"] = *res.ProcID
	}
	if res.Appname != nil {
		resMap["appname"] = *res.Appname
	}
	if res.MsgID != nil {
		resMap["msgid"] = *res.MsgID
	}
	if res.StructuredData != nil {
		resMap["structureddata"] = *res.StructuredData
	}
	return resMap, nil
}

func (m *EnterpriseLogProcessor) parseCSV(str string, dataMap map[string]interface{}) (res interface{}, err error) {
	err = nil
	newPart := new(CX10K)

	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	csvArray := strings.Split(str, ",")
	arrayLen := len(csvArray)
	if arrayLen < 25 {
		return nil, fmt.Errorf("unable to convert csv to json, message is too short, containing only %v values", len(csvArray))
	}

	newPart.GenTime = csvArray[0]
	newPart.Session.State = csvArray[1]
	newPart.Session.Action = csvArray[2]
	newPart.Source.Vrf = csvArray[3]
	newPart.Source.IP = csvArray[4]
	newPart.Source.Port = parseUint64(csvArray[5])
	newPart.Destination.IP = csvArray[6]
	newPart.Destination.Port = parseUint64(csvArray[7])
	newPart.Protocol = parseUint64(csvArray[8])
	newPart.Session.ID = csvArray[9]
	newPart.Security.PolicyID = csvArray[10]
	newPart.Security.RuleID = csvArray[11]
	newPart.Security.RuleName = csvArray[12]
	newPart.Session.IflowPkts = parseUint64(csvArray[13])
	newPart.Session.IflowBytes = parseUint64(csvArray[14])
	newPart.Session.RflowPkts = parseUint64(csvArray[15])
	newPart.Session.RflowBytes = parseUint64(csvArray[16])
	newPart.Source.Vlan = parseUint64(csvArray[17])
	newPart.Dsx.Type = csvArray[18]
	newPart.Dsx.SwVer = csvArray[19]
	newPart.Dsx.SerNum = csvArray[20]
	newPart.Dsx.DeviceName = csvArray[21]
	newPart.Dsx.UnitID = csvArray[22]
	newPart.Version = csvArray[23]
	newPart.Security.PolicyName = csvArray[24]

	if arrayLen >= 25 && arrayLen < 32 {
		return nil, fmt.Errorf("Too short, unable to process csv data from %v, using version %v, containing %v values", newPart.Dsx.DeviceName, newPart.Dsx.SwVer, len(csvArray))
	}

	if arrayLen >= 32 {
		newPart.Security.DisplayName = csvArray[25]
		newPart.Nat.SourceXltdIP = csvArray[26]
		newPart.Nat.DestXltdIP = csvArray[27]
		newPart.Nat.DestXltdPort = parseUint64(csvArray[28])
		newPart.Security.Encrypted = csvArray[29]
		newPart.Session.Direction = csvArray[30]
		newPart.Session.Create = csvArray[31]
	}

	if arrayLen >= 33 && arrayLen < 37 {
		return nil, fmt.Errorf("Too short, unable to process csv data from %v, using version %v, containing %v values", newPart.Dsx.DeviceName, newPart.Dsx.SwVer, len(csvArray))
	}

	if arrayLen >= 37 {
		newPart.Session.Close = csvArray[32]
		newPart.Source.VrfName = csvArray[33]
		newPart.Destination.VrfName = csvArray[34]
		newPart.Destination.Vrf = csvArray[35]
		newPart.Destination.Vlan = parseUint64(csvArray[36])
	}

	if arrayLen > 37 {
		numberOfExtraFields := arrayLen - 37
		m.log.Infof("Extra %v fields received from %v, using version %v", numberOfExtraFields, newPart.Dsx.DeviceName, newPart.Dsx.SwVer)
	}

	newPart.Tags = []string{}
	newPart.SyslogPriority = dataMap["priority"].(uint8)
	newPart.SyslogVersion = dataMap["version"].(uint16)
	newPart.Hostname = dataMap["hostname"].(string)
	newPart.ProcName = dataMap["appname"].(string)
	newPart.ProcID = dataMap["procid"].(string)
	if dataMap["msgid"] != nil {
		newPart.MsgID = dataMap["msgid"].(string)
	}
	if dataMap["structureddata"] != nil {
		newPart.Sd = dataMap["structureddata"].(string)
	}

	return *newPart, err
}
