package processor

import (
	"errors"
	"encoding/json"
	"net/http"
	"bytes"
	"crypto/tls"
	"time"
	"fmt"
	"sync"

  "github.com/Jeffail/benthos/v3/public/bloblang"
	"github.com/ReneKroon/ttlcache/v2"
)

type AuthRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Tenant   string `json:"tenant"`
}

func init() {
	fmt.Println("Initializing Token Cache")
	//create cache
	tokenCache := makeCache()
	//create lock for multiple pollers
	var lock = sync.RWMutex{}
	//create http client
	client := &http.Client{
						Transport: &http.Transport{
									 TLSClientConfig: &tls.Config{
													 InsecureSkipVerify: true,
									},
						},
						Timeout: 15 * time.Second,
	}

	//create client
	bloblang.RegisterFunction("get_psm_credentials", func(args ...interface{}) (bloblang.Function, error) {
		// Expect all the params to be provided
		if len(args) != 4 {
			return nil, errors.New("Not enough arguments to get PSM auth token.")
		}
		//set values
		var url string
		var username string
		var password string
		var tenant string
		//parse values
		if err := bloblang.NewArgSpec().
							StringVar(&url).
							StringVar(&username).
							StringVar(&password).
							StringVar(&tenant).
							Extract(args); err != nil {
			return nil, err
		}
		//make request body
		authRequest := &AuthRequest{
			Username:   username,
			Password: password,
			Tenant: tenant,
		}
		return func() (interface{}, error) {
			lock.Lock()
			defer lock.Unlock()
			//try and fget token from cache to avoid spamming psm
			if cachedHeader, exists := tokenCache.Get(url); exists == nil {
				return cachedHeader, nil
			}
			//token not found in cache, call getToken
			if header, err := getAuthHeaderFromPSM(*client, url, *authRequest); err == nil {
				tokenCache.SetWithTTL(url, header, 1*time.Hour)
				fmt.Printf("New PSM Token: %v for PSM: %v\n", header, url)
				return header, nil
			} else {
				return nil, err
			}
		}, nil
	})
}

func makeCache() *ttlcache.Cache {
	cache := ttlcache.NewCache()

	expirationCallback := func(key string, reason ttlcache.EvictionReason, value interface{}) {
		fmt.Printf("PSM Token has expired for: (%s)\n", key)
	}
	cache.SetExpirationReasonCallback(expirationCallback)
	cache.SetCacheSizeLimit(100)
	cache.SkipTTLExtensionOnHit(true)
	return cache;
}

func getAuthHeaderFromPSM(client http.Client, url string, request AuthRequest) (interface{}, error) {
	requestBody, _ := json.Marshal(request)

	req, err := http.NewRequest("POST", "https://" + url +"/v1/login", bytes.NewBuffer(requestBody))
	resp, err := client.Do(req)

	if err != nil {
			fmt.Println("Error during HTTP post:", err)
			return nil, err
	}

	if resp.StatusCode != http.StatusOK {
    fmt.Println("Failed to get Token:", resp.StatusCode)
    // You may read / inspect response body
    return nil, errors.New("Bad response code during authenticate call:" + string(resp.StatusCode))
}
	defer resp.Body.Close()
	auth_header := resp.Header.Get("Set-Cookie")
	return auth_header, nil
}
