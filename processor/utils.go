package processor

import (
	"strconv"
	"time"

	"github.com/Jeffail/benthos/v3/lib/message/tracing"
	"github.com/Jeffail/benthos/v3/lib/processor"
	"github.com/Jeffail/benthos/v3/lib/types"
	opentracing "github.com/opentracing/opentracing-go"
	olog "github.com/opentracing/opentracing-go/log"
)

func IterateMessages(
	operationName string, msg types.Message,
	iter func(int, opentracing.Span, types.Part) error,
) {
	exec := func(i int) {
		part := msg.Get(i)
		span := tracing.GetSpan(part)
		if span == nil {
			span = opentracing.StartSpan(operationName)
		} else {
			span = opentracing.StartSpan(
				operationName,
				opentracing.ChildOf(span.Context()),
			)
		}
		if err := iter(i, span, part); err != nil {
			processor.FlagErr(part, err)
			span.LogFields(
				olog.String("event", "error"),
				olog.String("type", err.Error()),
			)
		}
		span.Finish()
	}
	for i := 0; i < msg.Len(); i++ {
		exec(i)
	}
}

//CreateFilterSet turns a list of filtered users into a map for access
func CreateFilterSet(list []string) map[string]bool {
	set := make(map[string]bool)
	for _, v := range list {
		set[v] = true
	}
	return set
}

//GetTimeStamp return current timestamp in millis
func GetTimeStamp() int64 {
	return time.Now().UnixNano() / 1e6
}

func parseUint64(str string) uint64 {
	i, err := strconv.ParseUint(str, 10, 64)
	if err != nil {
		return 0
	}
	return i
}
