package processor

import (
	"errors"
	"time"
	"encoding/json"

	"github.com/Jeffail/benthos/v3/lib/log"
	"github.com/Jeffail/benthos/v3/lib/metrics"
	"github.com/Jeffail/benthos/v3/lib/processor"
	"github.com/Jeffail/benthos/v3/lib/types"
	"github.com/gofrs/uuid"
	opentracing "github.com/opentracing/opentracing-go"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	jsoniter "github.com/json-iterator/go"
)

//------------------------------------------------------------------------------

func init() {
	processor.RegisterPlugin(
		"cloud-event-encoder",
		func() interface{} {
			return NewCloudEventEncoderProcessorConfig()
		},
		func(
			iconf interface{},
			mgr types.Manager,
			logger log.Modular,
			stats metrics.Type,
		) (types.Processor, error) {
			conf, ok := iconf.(*CloudEventEncoderProcessorConfig)
			if !ok {
				return nil, errors.New("Failed to cast config for cloud-event-encoder processor.")
			}
			return NewCloudEventEncoderProcessor(*conf, logger, stats)
		},
	)
	processor.DocumentPlugin(
		"cloud-event-encoder",
		`Converts a message to cloudevent format.`,
		nil,
	)
}

// NewCloudEventEncoderProcessorConfig creates a config with default values.
func NewCloudEventEncoderProcessorConfig() *CloudEventEncoderProcessorConfig {
	return &CloudEventEncoderProcessorConfig{
		SpecVersion:  "1.0",
		Hostname:  "localhost",
		IP:        "127.0.0.1",
		DataEncoding: "json",
	}
}

// CloudEventEncoderProcessorConfig contains config fields for our plugin type.
type CloudEventEncoderProcessorConfig struct {
	SpecVersion string `json:"specversion" yaml:"specversion"`
	Hostname      string   `json:"hostname" yaml:"hostname"`
	IP            string   `json:"ip" yaml:"ip"`
	MessageType 	string 	 `json:"messagetype" yaml:"messagetype"`
	DataContentType string `json:"datacontenttype" yaml:"datacontenttype"`
	DataEncoding string `json:"dataencoding" yaml:"dataencoding"`
}

//------------------------------------------------------------------------------

// CloudEventEncoderProcessor is a processor that reads all mqtt messages.
type CloudEventEncoderProcessor struct {
	specversion 			 string
	source             string
	messageType        string
	datacontenttype 	 string
	dataencoding 			 string
	marshaller         jsoniter.API
	log                log.Modular
	stats              metrics.Type
	mCount             metrics.StatCounter
	mDropped           metrics.StatCounter
	mBatchSent         metrics.StatCounter
	mFiltered          metrics.StatCounter
}

// NewCloudEventEncoderProcessor returns a CloudEventEncoderProcessor processor.
func NewCloudEventEncoderProcessor(
	conf CloudEventEncoderProcessorConfig, log log.Modular, stats metrics.Type,
) (types.Processor, error) {
	m := &CloudEventEncoderProcessor{
		specversion:   conf.SpecVersion,
		source:        conf.Hostname + "@" + conf.IP,
		messageType:	 conf.MessageType,
		datacontenttype: conf.DataContentType,
		dataencoding: conf.DataEncoding,
		marshaller: jsoniter.ConfigFastest,
		log:           log,
		stats:         stats,
		mCount:        stats.GetCounter("count"),
		mDropped:      stats.GetCounter("dropped"),
		mBatchSent:    stats.GetCounter("batch.sent"),
	}
	return m, nil
}

// ProcessMessage applies the processor to a message
func (m *CloudEventEncoderProcessor) ProcessMessage(msg types.Message) ([]types.Message, types.Response) {
	// Always create a new copy if we intend to mutate message contents.
	newMsg := msg.Copy()
	proc := func(index int, span opentracing.Span, p types.Part) error {
		defer m.recover(p)
		m.mCount.Incr(1)
		m.log.Debugf("Received message: %s with metadata: %v\n", p.Get(), p.Metadata())

		m.encodeInCloudEvent(p)

		m.log.Debugf("Sending message: %s with metadata: %v\n", p.Get(), p.Metadata())
		return nil
	}
	IterateMessages("cloud-event-encoder", newMsg, proc)
	m.mBatchSent.Incr(1)
	msgs := [1]types.Message{newMsg}
	return msgs[:], nil
}

// CloseAsync shuts down the processor and stops processing requests.
func (m *CloudEventEncoderProcessor) CloseAsync() {
}

// WaitForClose blocks until the processor has closed down.
func (m *CloudEventEncoderProcessor) WaitForClose(timeout time.Duration) error {
	return nil
}

func (m *CloudEventEncoderProcessor) recover(p types.Part) {
	if r := recover(); r != nil {
		m.log.Errorf("Recovered from a message processing error.", r)
		processor.FlagFail(p)
		m.mDropped.Incr(1)
	}
}

func (m *CloudEventEncoderProcessor) encodeInCloudEvent(p types.Part) {
	event :=  cloudevents.NewEvent()
	event.SetType(m.messageType)
	event.SetSpecVersion(m.specversion)
	event.SetSource(m.source)
	event.SetID(uuid.Must(uuid.NewV4()).String())
	event.SetTime(time.Now())
	event.SetDataContentType(m.datacontenttype)

	if m.dataencoding == "json" {
		var objmap map[string]interface{}
		err := json.Unmarshal(p.Get(), &objmap)
		if err != nil {
			m.recover(p)
		}
		event.SetData(cloudevents.ApplicationJSON, objmap)
	} else {
		event.SetData(cloudevents.ApplicationJSON, p.Get())
	}
	marshalledData, err := m.marshaller.Marshal(&event)
	if err == nil {
		p.Set([]byte(marshalledData))
		return
	}
	m.recover(p)
}
