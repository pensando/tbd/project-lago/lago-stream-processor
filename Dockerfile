FROM golang:1.17.9 AS build

RUN useradd -u 10001 lago

WORKDIR /go/src/lago/
COPY . /go/src/lago/

ENV GO111MODULE on
RUN make build-static

FROM golang:1.17.9 AS package

WORKDIR /

COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /go/src/lago/bin/lago .

RUN apt-get update && apt-get install -y curl util-linux && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

USER lago

EXPOSE 4195

CMD ["/lago", "-c", "/etc/lago/baseconfig/config.yaml", "streams", "/etc/lago/config"]
