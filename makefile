.PHONY: all build docker clean test test-race test-integration lint

TAGS =

INSTALL_DIR        = $(GOPATH)/bin
DEST_DIR           = ./bin

IMAGE_NAME ?= pensando-lago
VERSION ?= latest
DATE      := $(shell date +"%Y-%m-%dT%H:%M:%SZ")

VER_FLAGS = -X github.com/Jeffail/benthos/v3/lib/service.Version=$(VERSION) \
	-X github.com/Jeffail/benthos/v3/lib/service.DateBuilt=$(DATE)

LD_FLAGS =
GO_FLAGS = -v -mod=vendor
DOCKER_FLAGS = CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GOPROXY=https://proxy.golang.org
DOCKER_LD_FLAGS = --extldflags "-static"

APPS = lago
all: $(APPS)

build:
	@go mod tidy
	@go mod vendor
	@mkdir -p $(dir $@)
	@go build $(GO_FLAGS) -tags "$(TAGS)" -ldflags "$(LD_FLAGS) $(VER_FLAGS)" -o ${DEST_DIR}/${APPS}

build-static:
	@go mod tidy
	@go mod vendor
	@mkdir -p $(dir $@)
	@${DOCKER_FLAGS} go build $(GO_FLAGS) -tags "$(TAGS)" -ldflags "$(DOCKER_LD_FLAGS) $(VER_FLAGS)" -o ${DEST_DIR}/${APPS}

docker:
	@docker build . -t "$(IMAGE_NAME):$(VERSION)"

lint:
	@go vet $(GO_FLAGS) ./...
	@golint -min_confidence 0.5 ./processor/... .

test:
	@go test $(GO_FLAGS) -timeout 300s -short ./...

test-race:
	@go test $(GO_FLAGS) -timeout 300s -short -race ./...

test-integration:
	@go test $(GO_FLAGS) -timeout 600s ./...

clean:
	rm -rf $(DEST_DIR)
