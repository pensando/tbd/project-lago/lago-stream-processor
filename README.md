# Project Lago
##Summary:
This repository contains source code for Lago, the stream processor for pensando data.

#Requirements:
Setup docker repository access credentials

##How To Build Locally:
make build


##How To Build Docker Image:
1. make docker
2. docker tag pensando-lago:latest registry.gitlab.com/pensando/tbd/project-lago/lago-stream-processor:$ver
3. docker push registry.gitlab.com/pensando/tbd/project-lago/lago-stream-processor:v0.6

##Running Locally:
1. make build
2. ./bin/lago -c config.yaml streams streams/


##Modules Under Processor
Auth - Handles authentication with PSM

Cloud Event Encoder - Optional encoding of data into cloudevents

Enterprise Event Parser - Parsing for DSS Messages

Enterprise Event Splitter - Splitter for DSS Messages

Firewall Log Parser - Parser for DSC Messages

Structs - Object Defenitions

Utils - Various utils to make iteration easier.
