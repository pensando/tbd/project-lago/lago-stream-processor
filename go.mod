module gitlab.com/pensando/tbd/project-lago/lago-stream-processor

go 1.15

require (
	github.com/Jeffail/benthos/v3 v3.65.0
	github.com/ReneKroon/ttlcache/v2 v2.3.0
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/influxdata/go-syslog/v3 v3.0.0
	github.com/json-iterator/go v1.1.11
	github.com/opentracing/opentracing-go v1.2.0
	go.uber.org/automaxprocs v1.3.0
)
